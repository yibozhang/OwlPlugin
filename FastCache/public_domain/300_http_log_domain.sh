#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_http_log_domain.sh
# Revision:     v0.1
# Date:         2016/12/20
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Metric value definition:
# 	0: normal
# 	1: Trigger the alarm
# -------------------------------------------------------------------------------
# Revision v0.1
# 实现每5分钟统计日志抓取502和404超过总访问的10%报警
# -------------------------------------------------------------------------------

function http_status_count() {
    # Time:
    local TODAY=$(date +%Y%m%d)
    local HOUR=$(date +'%Y%m%d%H')
    local SECOND=$(date +'%Y:%H:%M:%S')
    local FIVE_SECOND_AGO=$(date -d "5 minute ago" +'%Y:%H:%M:%S')
   
    # log:
    local LOG_PATH=/cache/logs/heka-data
    local LOG=$LOG_PATH/$TODAY/${HOUR}.log
    
    http_status_codes=(`cat ${LOG} | grep 'kwmov.a.yximgs.com' | \
        awk -F'[/ "]' '$6>="'"$FIVE_SECOND_AGO"'" && $6<="'"$SECOND"'"' | \
	grep -ioE "HTTP\/1\.[1|0]\"[[:blank:]][0-9]{3}" | \
        awk '{   
                if($2==404)  
                    {i++}  
                else if($2==502)  
                    {j++}
                else if($2 >=100 && $2 <=600)
                    {k++}  
        }END{  
        # 判断 i存在输出i,否则输出0  
         print i?i:0,j?j:0,k?k:0
        }'  
        `)

    echo ${http_status_codes[@]}
}

function alarm_502 {
    local HTTP_CODE=(`http_status_count`)
    local HTTP_CODE_502=${HTTP_CODE[1]}
    local HTTP_CODE_ALL=${HTTP_CODE[2]}

    # > 0.1 means more than 10%. +1 for prevent divide by zero.
    local PERCENT_502_FALOT=$(echo "scale=2; $HTTP_CODE_502 / ($HTTP_CODE_ALL + 1)*100"|bc)
    local PERCENT_502=`echo $PERCENT_502_FALOT | awk '{printf("%d\n",$1)}'`

    if [ "${PERCENT_502}" -gt "10" ] ; then
        echo Error 
        return 1
    else
        echo OK
        return 0
    fi
}

function alarm_404 {
    local HTTP_CODE=(`http_status_count`)
    local HTTP_CODE_404=${HTTP_CODE[0]}
    local HTTP_CODE_ALL=${HTTP_CODE[2]}

    # > 0.1 means more than 10%. +1 for prevent divide by zero.
    local PERCENT_404_FALOT=$(echo "scale=2; $HTTP_CODE_404 / ($HTTP_CODE_ALL + 1)*100"|bc)
    local PERCENT_404=`echo $PERCENT_404_FALOT | awk '{printf("%d\n",$1)}'`

    if [ "${PERCENT_404}" -gt "10" ] ; then
        echo Error 
        return 1
    else
        echo OK
        return 0
    fi
}

# Call function
MSG_502=$(alarm_502)
RETVAL_502=$?
MSG_404=$(alarm_404)
RETVAL_404=$?
DATE=`date +%s`
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF
[
  {
  "endpoint"   : "$HOST",
  "tags"       : "$TAG",
  "timestamp"  : $DATE,
  "metric"     : "http.log.yximgs.502",
  "value"      : $RETVAL_502,
  "counterType": "GAUGE",
  "step"       : 300},
  {
  "endpoint"   : "$HOST",
  "tags"       : "$TAG",
  "timestamp"  : $DATE,
  "metric"     : "http.log.yximgs.404",
  "value"      : $RETVAL_404,
  "counterType": "GAUGE",
  "step"       : 300
  }
]
EOF

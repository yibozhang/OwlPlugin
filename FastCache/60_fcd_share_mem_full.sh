#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_fcd_share_mem_full.sh
# Revision:     v0.1
# Date:         2016/12/16
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Metric value definition:
# 	0: successfully allocate memory 
# 	1: failure allocate memory 
# -------------------------------------------------------------------------------
# Revision v0.1
# Description:	Fastcache 无法分配共享内存
# -------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fcd_share_mem_full()
{
    local FCD_CONF=/usr/local/fastcache/etc/fastcache.conf
    local FINFO_BIN=/usr/local/fastcache/bin/finfo    
    local STAT_LOG=`cat $FCD_CONF | grep '^server.stat_runtime_log' | \
		awk -F'=' '{print $2}' | sed -r 's/.*\"(.+)\".*/\1/'`
    local FINFO_CMD="$FINFO_BIN -S $STAT_LOG -o"
    local TOTAL_SIZE=`$FINFO_CMD | grep 'Total Size' | awk -F '|' '{print $5}' | awk '{print $1}'`
    local STORAGES_SIZE=`$FINFO_CMD | grep 'Storages Size' | awk -F '|' '{print $5}' | awk '{print $1}'`
    local FREE_SIZE=`$FINFO_CMD | grep 'Free' | fgrep 'Split Failed'| awk '{print $12}'`
    local THRESHOLD=`expr $(($STORAGES_SIZE * 2)) - $FREE_SIZE`

    if [ ! -f "$FCD_CONF" ]; then
        echo $"$FCD_CONF 配置文件不存在,若不是FCD设备请忽略"
        return 1
    elif [ $THRESHOLD -gt $TOTAL_SIZE ]; then
        echo "failure allocate memory"
        return 1
    else
        echo "successfully allocate memory"
        return 0
    fi
}

# Call function
MSG=$(fcd_share_mem_full)
RETVAL=$?
DATE=`date +%s`
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$HOST",
    "tags"       : "$TAG",
    "timestamp"  : $DATE,
    "metric"     : "fcd.share.mem.full",
    "value"      : $RETVAL,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF

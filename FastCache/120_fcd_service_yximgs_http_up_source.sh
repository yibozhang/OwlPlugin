#!/bin/bash
#===============================================================================
#   DESCRIPTION:cache 快手域名 fcd回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/2 
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function god_ip_alive
{
        local value=$(httping $ip -N 1 -c10 -t2 -f > /dev/null;echo $?) 

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"yximgs_alive=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fcd.http.up.alive\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, 
         
}
function httpingloss
{
       local value=$(httping $ip -c10 -t2 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"yximgs_loss=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fcd.http.upstreams.loss\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},  
}
function httpingms
{
        local value=$(httping $ip -c10 -t2 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')
        if [[ -n $value ]];then

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"yximgs_ms=$ip\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.fcd.http.upstreams.ms\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, >> /tmp/.FCDRETURN_upstreams_httping.tmp 
fi
}
#####################################################
FCD=$(ps -ef |grep fastcache|wc -l)
if [ ${FCD} -gt 0 ];then
[ ! -x  /usr/bin/httping ] &&  yum install -y httping-2.4
INNERDNS=$(grep 'server.dns' /usr/local/fastcache/etc/fastcache.conf|grep -v '#'|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
for INNER in ${INNERDNS[@]};do
        PERIP=$(host -W 3 kwmov.a.yximgs.com $INNER|grep address|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
done
	IPlist=$(echo "$PERIP" |sort|uniq -c|awk '{print $2}')
endpoint=$(hostname -s)
timestamp=$(date +%s)
#######使用多线程##################
    thread=10
    tmp_fifofile=/tmp/$$.fifo
    mkfifo $tmp_fifofile
    exec 6<>$tmp_fifofile
    rm $tmp_fifofile 

for ((i=0;i<$thread;i++));do
        echo 
done >&6

echo -n "["
 for ip in ${IPlist[@]};do 
    read -u6
    {
      god_ip_alive
      httpingloss
      httpingms
      echo >&6
    } &
  done
wait
  sed -ie 's/,$/]/' /tmp/.FCDRETURN_upstreams_httping.tmp 
  cat /tmp/.FCDRETURN_upstreams_httping.tmp 
  rm -f /tmp/.FCDRETURN_upstreams_httping.tmp 
exec 6>&-
exit 0
else
exit;
fi

#!/bin/bash
#-*- coding:utf8 -*-
# -------------------------------------------------------------------------------
# Filename:     60_check_traffic_gateway
# Revision:     1.0
# Date:         2016/12/09
# Author:       张医博
# Email:        zhangyb@fastweb.com.cn
# Description:  Statisical export network traffic
# FILE          网卡流量文件
# NETWORK       出口网卡名称
# TRAFFIC       获取出口网卡 in/out 流量
# TYPE          流量类型
# HOSTNAMES     主机名称
# TIMESTAMP     时间戳
#--------------------------------------------------------------------------------
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

# 主函数
function main() {

FILE='/proc/net/dev'
NETWORK=$(ip r | awk '/^default/{print $5}' | head -1)
TRAFFIC=$(cat ${FILE} | awk -F: -v IFACE=${NETWORK} '{if($1~IFACE) { print $2}}' | awk '{print $1,$9}')
ALLBITS=($(echo ${TRAFFIC[@]} | awk '{print $1*8,$2*8}'))
TYPE=(net.if.in.bits net.if.out.bits)
HOSTNAMES=$(hostname -s)
TIMESTAMP=$(date +%s)
SIGIN=(",")

echo "["
for i in  $(seq 0 $[${#TYPE[@]}-1])
do
cat << EOF
    {
        "endpoint"      :"${HOSTNAMES}",
        "tags"          :"iface=gateway",
        "timestamp"     :${TIMESTAMP},
        "metric"        :"${TYPE[$i]}",
        "value"         :${ALLBITS[$i]},
        "counterType"   :"COUNTER",
        "step"          :60
    }${SIGIN[$i]}
EOF
done
echo "]"

}

#入口
main

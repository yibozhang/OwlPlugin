#!/bin/bash
#   DESCRIPTION:华数博睿url热度监测 
#       设备组:vfcc 
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/9/26 18:22
#================================

check_vfcc_wasu(){
        for URL in ${WASUURL[@]} ; do
        curl -I $URL -x 127.0.0.1:80 >> /tmp/001.tmp 2>&1
        done
        SUM=$(cat /tmp/001.tmp|grep -c 'X-Cache: HIT')
        return $SUM
}

#每分钟过滤访多玩超父访问日志4xx或5xx的状态数据
check_fcd_wasu(){
        for URL in ${WASUURL[@]} ; do
        curl -I $URL -x 127.0.0.1:80 >> /tmp/001.tmp 2>&1
        done
        SUM=$(cat /tmp/001.tmp|grep -cE 'hit|X-Cache: HIT')
        return $SUM
}
##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
fcd=`pgrep fastcache|wc -l`
rm -f /tmp/001.tmp
WASUURL=(http://wasu.cloudcdn.net/data10/ott/345/2014-07/06/1404649462362_990132.ts http://wasu.cloudcdn.net/data10/ott/347/2014-04/18/1397805183736_188514.ts)
endpoint=`hostname -s`
timestamp=`date +%s`

if [[ $vfcc -gt 0 ]]; then
    msg1=$(check_vfcc_wasu)
    value=$?
elif [ $fcd -gt 0 ]; then
    msg2=$(check_fcd_wasu)
    value2=$?
else
    exit;
fi
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.curl.wasu\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 30},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.curl.wasu\",\
        \"value\": $value2, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

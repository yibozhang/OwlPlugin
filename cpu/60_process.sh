#!/bin/bash

# Metric value definition:
# #process

# Get current process count.
#   Return an int to zabbix.
function proc_count() {

    local c=$(ls -d /proc/* | grep "/proc/[0-9]\+" | wc -l)

    if [ -z ${c} ] ; then
        local c=1000
    fi

    echo ${c}
    return 0
}

msg=$(proc_count)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.proc\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"

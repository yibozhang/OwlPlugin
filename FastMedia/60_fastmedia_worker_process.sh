#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_fastmedia_worker_process.sh
# Revision:     1.0
# Date:         2016/11/28
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  采集fastmedia work进程占用cpu百分比
#--------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fastmedia_worker_process()
{
CPUUSE=`/usr/bin/top -bcn 1 |grep fastmedia|awk '/worker/{count=NF-5;print $count}'`
echo $CPUUSE
}
#---------------------------------------------------------------------------------

# Call function
msg=$(fastmedia_worker_process)
date=`date +%s`
host=$HOSTNAME 
tag=""

# Send JSON message 
echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"fastmedia.worker.process\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"

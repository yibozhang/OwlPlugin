#!/bin/bash
#===========================================
#   DESCRIPTION:TCP state监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/6
#1.01:修改监控方式 ss -o 改为 ss -nat
#============================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`ss -ant state close-wait | wc -l`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"ss.close.wait\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]

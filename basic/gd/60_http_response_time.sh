#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_http_response_time.sh
# Revision:     1.0
# Date:         2016/08/03
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  采集本机 http port 80 的响应时间。（需要保证设备有安装 cURL）
# -------------------------------------------------------------------------------
# Revision 1.0
# 采集本机 http port 80 的响应时间。（需要保证设备有安装 cURL）
#FCD
#curl --connect-timeout 10 -m 10  -s -w %{time_total} -H "Host: fastweb.com.cn" "http://127.0.0.1/do_not_delete_noc/2k.jpg" 
#VFCC
#curl --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} -H "Host: update2.fastweb.cdn.qq.com"  "http://127.0.0.1/dltest/dltest.gif" 
# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

fcd=`pgrep fastcache|wc -l`
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`

endpoint=`hostname -s`

if [ $fcd -gt 0 ]; then
    value=`curl --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} -H "Host: fastweb.com.cn" "http://127.0.0.1/do_not_delete_noc/2k.jpg" 2> /dev/null`
elif [ $vfcc -gt 0 ]; then
    value=`curl --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} -H "Host: update2.fastweb.cdn.qq.com"  "http://127.0.0.1/dltest/dltest.gif" 2> /dev/null`
else
    value=0
fi

timestamp=`date +%s`
value=$(echo "$value*1000"|bc)
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"http.response.time\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]


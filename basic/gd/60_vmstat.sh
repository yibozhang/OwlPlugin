#!/bin/bash
#===========================================
#   DESCRIPTION:TCP state监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/15
# vmstat r / b 30 秒平均值
#============================================
#vmstat procs
#r: The number of processes waiting for run time.
#b: The number of processes in uninterruptible sleep.

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

s='{"endpoint":"%s","tags":"","timestamp":%s,"metric":"%s","value":%s,"counterType":"GAUGE","step":60}\n'
# first line takes values from vmstat
# second line sorts and removes the biggest and smallest
# calculates the average
count=30
vmstat 1 $count | tail -n$count |  awk '{print $1, $2}' \
| sort -nk1 | sed -e '1d' -e '$d' \
| awk '{r+=$1; b+=$2} END{print "vmstat.procs.r", r/NR;print "vmstat.procs.b", b/NR}' \
| awk -v date="$(date +%s)" -v hostname="$(hostname -s)" '{print hostname, date, $0}' \
| awk -v format=$s '{printf(format,$1,$2,$3,$4) }' \
| awk -v size=2 'BEGIN{print "["}{if(NR<size){print $0","} else {print $0}}END{print "]"}'

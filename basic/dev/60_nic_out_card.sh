#!/bin/bash
# jiangbin
# 2016-6-22
# print default_out_interface

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`awk '$2 == 00000000 { print $1 }' /proc/net/route`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"nic.out.card\", \"value\": \"$value\", \"counterType\": \"GAUGE\", \"step\": 60}]

#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Found core file

# Monitor core file in /cache/logs:
#   return the corefile name str to zabbix if found, otherwise return "OK".

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function core_file_mon() {

    local core_file_dirs="/cache/logs/"

    # Find core file:
    for core_file_dir in ${core_file_dirs} ; do

        if [ ! -d "${core_file_dir}" ] ; then
            continue
        fi

        local core_file=$(cd /tmp && find ${core_file_dir} -maxdepth 1 -type f -name "core.*" -mtime 0 | head -1)
        if [ -n "${core_file}" ] ; then
            echo "Found <$(basename ${core_file})> in ${core_file_dir}"
            return 1
        fi

    done

    echo "OK"
    return 0

}

msg=$(core_file_mon)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.core_file\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"

#!/bin/bash
#-*- coding:utf8 -*-
# 作者: 张医博 (zhangyb@fastweb.com.cn)
# 复审: 白金 (platinum@fastweb.com.cn)
# 描述: 统计设备各类型流量
# 更新: 2016-12-07
# 说明: 按照 bps 单位上传数据（非 Bytes/s）

# 变量说明
# TYPES     网络流量类型数组集合
# DIRECT    网络流量进出方向数组集合
# TAGS      统计单位数组集合 bps/pps
# HOSTNAME  设备名称

# 导出换系统环境变量
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

# 主函数入口，初始化全局变量
function main() {
    TAGS=(pps bps)
    DIRECTS=(out in)
    TYPES=(all multicast broadcast local private routed_all routed_line1 routed_line2 routed_line3 routed_other)
    HOSTNAME=$(hostname -s)

    getsystem
    makejson
    exit 0
}

# 获取 status 文件中的内核参数，获取同时判断统计开关是否开启
function getsystem() {
    SYSTEMARG=$(cat /proc/sys/net/traffic_counter/status 2>/dev/null | egrep -v "^$" 2>/dev/null )
    TURN=$(cat /proc/sys/net/traffic_counter/enabled 2>/dev/null | tr -d " ")

    if [ "${SYSTEMARG}" != "" ] && [ "${TURN}" == "1" ]; then
        echo 1 > /proc/sys/net/traffic_counter/reset
        return 0
    fi
    return 1
}

# 获取 traffic 系统内核参数对应的 value，如果获取为空直接输出 "0 0 0"
function dealwith() {
    local ARGS=$1
    local VALUE=$(echo "${SYSTEMARG}" | egrep "^${ARGS}" 2>/dev/null | awk '{for(i=2;i<=NF;i++){printf" %s",$i}}' 2>/dev/null)

    if [ "${VALUE}" != "" ]; then
        echo ${VALUE# }
    else
        echo "0 0 0"
    fi
}

# 计算 bps 速率，忽略分母是 0 的情况
# 计算 pps 速率，忽略分母是 0 的情况
function caculate() {
    local ARG1=$1
    local ARG2=$2
    local ARG3=$3
    local ARG4=$4

    if [ "${ARG4}" != "0" ]; then
        if [ "${ARG1}" == "pps" ]; then
            echo "${ARG2} ${ARG4}" | awk '{printf("%d", int($1 * 1000 / $2 + 0.5))}'
        elif [ "${ARG1}" == "bps" ]; then
            echo ${ARG3} ${ARG4} | awk '{printf("%d", int($1 * 8 * 1000 / $2 + 0.5))}'
        else
            echo 0
        fi
    else
        echo 0
    fi
}

# 拼装 Json 串，回传给 OWL Agent
# SIGN 用于标志 "," 结束的位置。COUNTER 累加变量
function makejson() {
    local DATE=$(date +%s)
    local COUNTER=1
    local LEN=$(echo "${#TYPES[*]} * ${#TAGS[*]} * ${#DIRECTS[*]}" | bc)

    echo "["
    for TYPE in ${TYPES[@]}
    do

        for DIRECT in ${DIRECTS[@]}
        do
            local ARGS=${TYPE}"_"${DIRECT}
            local ARRY=($(dealwith ${ARGS}))

            for TAG in ${TAGS[@]}
            do

                local RATE=$(caculate ${TAG} ${ARRY[0]} ${ARRY[1]} ${ARRY[2]})
                local SIGN=""

                if [ ${COUNTER} -lt ${LEN} ]; then
                    local SIGN=","
                    ((COUNTER++))
                fi
cat << EOF
    {
        "endpoint"      :"$HOSTNAME",
        "tags"          :"",
        "timestamp"     :$DATE,
        "metric"        :"net.traffic.${DIRECT}.${TYPE}.${TAG}",
        "value"         :${RATE},
        "counterType"   :"GAUGE",
        "step"          :60
    }${SIGN}
EOF
            done

        done

    done
    echo "]"
}

#入口
main


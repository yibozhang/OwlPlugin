#!/bin/env perl
# -------------------------------------------------------------------------------
# Filename:     60_service_boss_api_5xx.pl
# Revision:     1.0
# Date:         2016/11/23
# Author:       蒋彬
# Email:        jiangbin@fastweb.com.cn
# Description:  每1分钟分析一次/logs/nginx/cdncs-api.fastweb.com.cn.access.log 上报最近5分钟的5xx的次数
# -------------------------------------------------------------------------------
# Revision 1.1
# 每5分钟分析一次/logs/nginx/cdncs-api.fastweb.com.cn.access.log 上报最近5分钟的5xx的次数
# 每1分钟分析一次/logs/nginx/cdncs-api.fastweb.com.cn.access.log 上报最近1分钟的5xx的次数  wangwei@fastweb.com.cn
#
# -------------------------------------------------------------------------------

#########################################################################################################
# PACKAGE

use strict;
use warnings;
use POSIX qw(strftime);

# PACKAGE
#########################################################################################################


#########################################################################################################
# CONSTANT

my $BOSS_API_NGX_LOG = '/logs/nginx/cdncs-api.fastweb.com.cn.access.log';
my $BOSS_API_NGX_LOG_SEEK = '/tmp/owl_service_boss_api_5xx.seek';

# CONSTANT
#########################################################################################################


#########################################################################################################
# FUNCTION

# 获取seek日志里的位置
sub Read_seek_log {
    my ($SEEK_LOG) = @_;
    # 如果SEEK_LOG文件不存在,则创建一个,并返回0.
    # 如果SEEK_LOG文件为空,则输入0,并返回0.
    if ( (!-e $SEEK_LOG) or ( -s $SEEK_LOG eq 0 ) ) {
        open (my $fh,">","$SEEK_LOG") or die;
        select($fh);
        print "0";
        select(STDOUT);
        close $fh;
        return 0;
    }

    # 读取flag(获取上次的记录)
    open (my $fh,,"$SEEK_LOG") or die;
    my $pre_size = <$fh>;
    chomp($pre_size);
    close $fh;

    # 如果当前的seek小于上次的seek,说明文件被切割过了,所以要从0开始
    my $cur_size = -s $BOSS_API_NGX_LOG;
    if ($cur_size < $pre_size ){
        return 0;
    }else {
        return $pre_size;
    }
}

# 保存seek位置到日志里
sub Save_seek_log {
    my ($last_size,$SEEK_LOG) = @_;
    # 保存seek到SEEK_LOG文件
    open (my $fh,">","$SEEK_LOG") or die;
    select($fh);
    print "$last_size";
    select(STDOUT);
    close $fh;
}

# FUNCTION
#########################################################################################################


############################################## MAIN START ###############################################
# MAIN

############

# 获取本次的时间
my $now_time_sec = time();
chomp($now_time_sec);

# 获取上次保存的seek记录
my $seek_flag = Read_seek_log($BOSS_API_NGX_LOG_SEEK);

# 从seek位置读取所有日志
open(my $fh_read,,"$BOSS_API_NGX_LOG") ||die ;
seek $fh_read,$seek_flag,0;

# 初始化5xx计数器
my $count_5xx = 0;

while( <$fh_read> ){
    if( $_ =~ /^(\S+) (\S+) (\S+?) \[([^]]+)\] "(.*?)" (\S+)/ ) {
        if ($6 >= 500){
            $count_5xx++;
        }
    }
}

# 保存读取完后的seek记录
my $last_size = tell($fh_read);
Save_seek_log($last_size,$BOSS_API_NGX_LOG_SEEK);

# 输出json格式:
my $endpoint = readpipe("hostname -s");
chomp($endpoint);

print qq([{"endpoint": "$endpoint", "tags": "", "timestamp": $now_time_sec, "metric": "service.boss.api.5xx", "value": $count_5xx, "counterType": "GAUGE", "step": 60}]);

# MAIN
############################################### MAIN END ################################################

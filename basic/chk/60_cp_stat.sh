#!/bin/bash

# Metric value definition:
# 0: OK
# 1: ReadOnly
# 2: Not Mounted

# Check /etc/fstab and mounted partition.
#   Return error str if partition not mounted or readonly, otherwise return str "OK".

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function cp_stat() {

    # Test rootfs first:
    touch "/.FS_TEST_BY_ZABBIX_AGENT.file" 2> /dev/null
    if [ $? -ne 0 ] ; then
        echo "/ RootFS ReadOnly"
        return 1
    fi

    # Test Each cache partition:
    for fs in $(grep -v "^ *#" /etc/fstab | awk '$2~/^\/cache/{print $2}') ; do

        mount | grep -q "$fs"
        if [ $? -ne 0 ] ; then
            echo "${fs} Not Mounted"
            return 2
        fi


        touch "${fs}/.FS_TEST_BY_ZABBIX_AGENT.file" 2> /dev/null
        if [ $? -ne 0 ] ; then
            echo "${fs} ReadOnly"
            return 1
        fi

    done

    echo "OK"
    return 0
}
function cp_stat_vfcc() {

    # Test rootfs first:
    touch "/.FS_TEST_BY_ZABBIX_AGENT.file" 2> /dev/null
    if [ $? -ne 0 ] ; then
        echo "/ RootFS ReadOnly"
        return 1
    fi

    # Test Each cache partition:
    for fs in $(grep -v "^ *#" /etc/fstab | awk '$2~/^\/cache|data/{print $2}') ; do

        mount | grep -q "$fs"
        if [ $? -ne 0 ] ; then
            echo "${fs} Not Mounted"
            return 2
        fi


        touch "${fs}/.FS_TEST_BY_ZABBIX_AGENT.file" 2> /dev/null
        if [ $? -ne 0 ] ; then
            echo "${fs} ReadOnly"
            return 1
        fi

    done

    echo "OK"
    return 0
}
###################################
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
if [ $vfcc -gt 0 ];then
   mgs=$(cp_stat_vfcc)
   retval=$?
 else
   mgs=$(cp_stat)
   retval=$? 
fi
# Call function
#msg=$(cp_stat)
#retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Parse message
# if [ 0 != "$retval" ] ; then
#     tag=$(echo $msg | awk '{print "partition=" $1}')
# fi

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"dev.cache_partition\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"

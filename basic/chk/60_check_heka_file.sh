#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Error

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function heka_enabled {
    grep -q "^server.access_log.*hekad.*$" /usr/local/fastcache/etc/fastcache.conf > /dev/null 2>&1
    return $?
}

function heka_file {
    ls -d /cache/logs/cronolog/* | wc -l
}

function heka_file_check {
    if heka_enabled && [ "`heka_file`" -eq 0 ]; then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}


# Call function
msg1=$(heka_file_check)
retval1=$?

date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.file\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
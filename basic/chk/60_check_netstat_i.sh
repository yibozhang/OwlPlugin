#!/bin/bash
#===============================================================================
#   DESCRIPTION: 检查 netstat -i 中 ERR, DRP, OVR 报文增量
#        AUTHOR: 白金
#       CREATED: 2016/12/27 12:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

DATAFILE="/dev/shm/netstat-i"

function main() {
    get_netstat_data
    compute_increment
    makejson
}

function get_netstat_data() {
    NETSTAT_DATA_CUR=($(netstat -i | grep 'RU$' | fgrep -v 'no statistics available' | awk '{sum_err += $5 + $9; sum_drp += $6 + $10; sum_ovr += $7 + $11} END {print sum_err, sum_drp, sum_ovr}'))
    if [ -f ${DATAFILE} ]; then
        NETSTAT_DATA_LAST=($(cat ${DATAFILE}))
    else
        NETSTAT_DATA_LAST=(${NETSTAT_DATA_CUR[@]})
    fi
    echo "${NETSTAT_DATA_CUR[@]}" > ${DATAFILE}
}

function compute_increment() {
    ERR_PKTS=$(echo "${NETSTAT_DATA_CUR[0]} - ${NETSTAT_DATA_LAST[0]}" | bc)
    DRP_PKTS=$(echo "${NETSTAT_DATA_CUR[1]} - ${NETSTAT_DATA_LAST[1]}" | bc)
    OVR_PKTS=$(echo "${NETSTAT_DATA_CUR[2]} - ${NETSTAT_DATA_LAST[2]}" | bc)
    NETSTAT_DATA_DETLA=(${ERR_PKTS} ${DRP_PKTS} ${OVR_PKTS})
}

function makejson() {
    HOST=$(hostname -s)
    DATE=$(date +%s)

    i=0
    echo '['

    CHAR=(',' ',' '')
    for TYPE in ERR DRP OVR; do
cat << EOF
    {
        "endpoint"      :"$HOST",
        "tags"          :"",
        "timestamp"     :$DATE,
        "metric"        :"netstat-i.${TYPE}",
        "value"         :${NETSTAT_DATA_DETLA[$i]},
        "counterType"   :"GAUGE",
        "step"          :60
    }${CHAR[$i]}
EOF
        ((i++))
    done

    echo ']'
}

main

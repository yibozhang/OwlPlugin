#!/bin/bash
#===============================================================================
#   DESCRIPTION:cache 回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/11/2 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function fpingloss
{
        rm -f /tmp/.fping_net.tmp
        echo -n "["
        for IP in ${IPlist[@]}
        do
        fping -A  -u -p 300 -c 100 $IP  >> /tmp/.fping_net.tmp 2>&1
        local value=`fgrep "$IP" /tmp/.fping_net.tmp |awk '{print $5}'|awk -F '/' '{print $3}'|cut -d "%" -f 1`

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"fpingloss=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.upstream.yy\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120},
        done
}
function fpingms
{
        for IP in ${IPlist[@]}
        do
        local value=`fgrep "$IP" /tmp/.fping_net.tmp |awk -F '/' '{print $9}'`

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"fpingms=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.upstream.yy\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 120}, >> /tmp/.fpingdw_net.tmp 

        done
        sed -ie 's/,$/]/' /tmp/.fpingdw_net.tmp 
        cat /tmp/.fpingdw_net.tmp 
        rm -f /tmp/.fpingdw_net.tmp 
}
#####################################################
[ -f /usr/local/fastcache/etc/fastcache.conf ] &&  C01_I02=$(grep -cE 'c01.i02|c02.p02' /usr/local/fastcache/etc/fastcache.conf)
FCD=$(ps -ef |grep fastcache|wc -l)
if [ ${C01_I02} -gt 0 -a $FCD -gt 0 ];then
[ ! -x  /usr/sbin/fping ] &&  yum install -y fping
INNERDNS=$(grep 'server.dns' /usr/local/fastcache/etc/fastcache.conf|grep -v '#'|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'|xargs fping|awk '/alive/ {print $1}')
for INNER in ${INNERDNS[@]};do
        PERIP=$(dig @$INNER w5.dwstatic.com |grep ser.ffdns.net|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')  2>&1
done
        IPlist=$(echo "$PERIP" |sort|uniq -c|awk '{print $2}')
endpoint=`hostname -s`
timestamp=`date +%s`

fpingloss
fpingms
 else
exit;
fi

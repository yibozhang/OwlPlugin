#!/bin/bash
#===============================================================================
#   DESCRIPTION:监控腾讯$HOST 域名是否正常! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

HOST=(dlied1 update2 dlsoftmgr)

if [ -f /etc/init.d/vfcc ];then
for domain in ${HOST[@]}
 do
        chck_url=`curl -s -I -H "X-Debug: True" -H "Host: $domain.fastweb.cdn.qq.com"  "http://127.0.0.1/dltest/dltest.gif"|grep HTTP|awk '{print $2}'`
        endpoint=`hostname -s`
        value=$chck_url
        timestamp=`date +%s`
        echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"vfcc=$domain\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.http\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
done
    else
        exit;
fi

#!/bin/bash
#===============================================================================
#   DESCRIPTION: vfcc 计费监控
#       设备组:C01_I01
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/9/13 17:16
#1.01 更新heka日志监控方法;更新crontab 监控方式
#===============================================================================
# 查看是否 active
check_active(){
    local ACTIVE=`/FastwebApp/fwutils/bin/fwhoami | grep ip_status | awk -F: '{print $2}'`
if [ $ACTIVE -eq 0 ]; then
    echo "暂停设备, 不需要检查"
else
    return $ACTIVE
fi
}

check_hekalog() {
    CNUM=$(ls  /cache/logs/log4stat |wc -l)
    return $CNUM
}

# 检查是否生成计数器文件
find_log(){
    SSUM=`ss -o state established '( dport = :http or sport = :http )'|wc -l`
if [ $SSUM -gt 150 ];then
    DNUM=$(ls /cache/logs/data | egrep tmp | wc -l)
    return $DNUM
else
        exit;
fi
}


# 检查是否挂载上传脚本
check_crontab(){
    fgrep -c upload_log_per5.pl /etc/cron.d/count_tencent
}

#############################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
endpoint=`hostname -s`
timestamp=`date +%s`

if [[ $vfcc -gt 0 ]]; then
    msg1=$(check_active)
    value=$?
    msg2=$(check_hekalog)
    value2=$?
    msg3=$(find_log)
    value3=$?
    msg4=$(check_crontab)
else
    exit;

fi
    
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"jifei=fwhoami\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.logs.jifei\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"jifei=log4stat\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.logs.jifei\",\
        \"value\": $value2, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"jifei=datalog\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.logs.jifei\",\
        \"value\": $value3, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"jifei=crontab\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"vfcc.service.logs.jifei\",\
        \"value\": $msg4, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

#!/bin/bash
#=================================
#   DESCRIPTION:bond 解析监控
#       设备组:vfcc 
# 
#        AUTHOR: weiqs
#       CREATED: 2016/11/9 16:31
#=================================

function dns_check {
    echo -n  "["
    for DOMAIN in ${nginx_domain[@]} 
 do
    dns_ip=$(host -W 1 $DOMAIN 127.0.0.1|grep -oP '(\d{1,3}\.){3}\d{1,3}' | tail -1 2>&1 )
    if [ -z $dns_ip ];then
      echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"bind_err=$DOMAIN\", \"timestamp\": $timestamp, \"metric\":\
          \"vfcc.service.check.bind\",\"value\": 1, \"counterType\": \"GAUGE\", \"step\": 300}, >>  /tmp/bindcheck.tmp
     else
       echo 0 > /dev/null 
    fi
done
        sed -ie 's/,$/]/' /tmp/bindcheck.tmp
        cat /tmp/bindcheck.tmp
        rm -f /tmp/bindcheck.tmp

}
########################
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
nginx_domain=$(cat /opt/vfcc*/nginx/conf/vhost.d/*.conf|grep server_name|awk '{print $2}'|sed 's/;$//'|sed 's/_//g'|grep -vE 'proxy*|manager.vfcc2.com|servername|*hongshiyun.com'|awk '!a[$0]++ {print $1}'|xargs)
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
endpoint=`hostname -s`
timestamp=`date +%s`
if [ $vfcc -gt 0 ];then
dns_check
else
exit;
fi

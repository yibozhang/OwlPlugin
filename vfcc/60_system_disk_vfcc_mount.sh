#!/bin/bash
#===============================================================================
#   DESCRIPTION:vfcc fstab ssd 写入监控，mount监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/17 11:22
#===============================================================================

check_fstab_rw()
{ 
  for ssd in $(grep -v "^ *#" /etc/fstab | awk '$2~/^\/wdata|hdata/{print $2}')
     do  grep $ssd /etc/fstab > /dev/null 

  if [ $? -ne 0 ] ; then
     echo "${fs} Not fstab"
     return 2
  fi
done
}
check_mount_ssd()
{
     for ssd in $(grep -v "^ *#" /etc/fstab | awk '$2~/^\/hdata|wdata/{print $2}')
     do  mount | grep -q "$ssd"

  if [ $? -ne 0 ] ; then
     echo "${fs} Not mount"
     return 2
  fi
done
}
###############
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [ $vfcc -gt 0 ];then

    msg=$(check_fstab_rw)
    value=$?
    msg1=$(check_mount_ssd)
    value1=$?

else

 exit 0

fi
echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"system.file.vfcc.fstab\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        \
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"system.disk.mount.check\",\
        \"value\": $value1, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

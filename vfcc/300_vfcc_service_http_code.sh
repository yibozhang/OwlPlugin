#!/bin/bash
#=================================
#   DESCRIPTION:log监控 
#       设备组:vfcc 
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/9/6 16:31
#v 1.01 修改时间条件，直接cat 日志
#v 1.02 增加favicon.ico 过滤，减少误报
#v 1.03 增加fwxgx.com未上量域名 过滤，减少误报
#v 1.04 用find 查找访问日志;过滤 _.access.log|proxy.parent|purge.mon 域名
#v 1.05 修改10分钟统计一次
#1216 修改超父计算方法
#================================

#每分钟过滤vfcc 访问日志4xx或5xx的状态数据并排除到华数401 405防盗链，减少误报
check_vfcc_http404(){
        local SUM=$(grep $TIME $HEKA_FILE|egrep -vE 'HEAD|favicon.ico|fwxgx.com'|awk '$9~/40.|50./ {print $9}'|egrep -vE '40[0-3]|405'|wc -l)
        return $SUM
}

#每分钟过滤访多玩超父访问日志4xx或5xx的状态数据
check_s01_p02_gd_http(){
        if [ -f /var/named/dwstatic.com ];then
         local SUM=$(grep $TIME $HEKA_FILE|grep -vE 'HEAD|favicon.ico|fwxgx.com'|awk -F "#_#" '/dwstatic.com/ $6~/40.|50./ {print $6}'|wc -l)
         return $SUM 
        else
          exit 0
        fi
}
##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
TODATA=$(date +%Y%m%d)
nowtime=$(date +%Y%m%d%H)
TIME=$(date -d "5  minutes ago" +%Y:%H:%M|sed 's/.$/\[0-9\]/')
HEKA_FILE=/cache/logs/heka-data/$TODATA/$nowtime.log
endpoint=`hostname -s`
timestamp=`date +%s`
if [[ $vfcc -gt 0 ]]; then
    msg1=$(check_vfcc_http404)
    value=$?
    msg2=$(check_s01_p02_gd_http)
    value2=$?
else
    exit;
fi
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"code=4xx5xx\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.logs.http.code\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 300},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"Domain=dwstat.com\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.logs.http.code\",\
        \"value\": $value2, \
        \"counterType\": \"GAUGE\", \"step\": 300}]
